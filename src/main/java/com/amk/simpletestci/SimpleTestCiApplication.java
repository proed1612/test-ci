package com.amk.simpletestci;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleTestCiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SimpleTestCiApplication.class, args);
	}
}
